/* jshint -W097 */
'use strict';

var setup = require('./setup'),
    repo = require('../models/repo'),
    sut = require('../lib/skill.denormalizer');

describe('when a skill is deleted', function () {

    var description = setup.create.skill.description(' ' + setup.create.random() + ' '),
        added = setup.create.skill.args.add(description.value);

    before(function (done) {
        sut.handleAdded(added, function(){
            sut.handleDeleted(added, done);
        });
    });

    it('should remove the associated read model', function (done) {
        repo.skills.findOne({description: description.sanitized}, function(err, found){
            expect(found).to.not.exist;
            done();
        });
    });


    after(function(done){
        repo.skills.remove({description: description.sanitized}, done);
    });

});

