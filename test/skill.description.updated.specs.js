/* jshint -W097 */
'use strict';

var setup = require('./setup'),
    repo = require('../models/repo'),
    sut = require('../lib/skill.denormalizer');

describe('when a skill description is updated', function () {

    var oldDescription = setup.create.skill.description(' ' + setup.create.random() + ' '),
        newDescription = setup.create.skill.description(' ' + setup.create.random() + ' '),
        added = setup.create.skill.args.add(oldDescription.value),
        updated = setup.create.skill.args.updateDescription().from(oldDescription.value).to(newDescription.value).literal;

    before(function (done) {
        sut.handleAdded(added, function(){
            sut.handleDescriptionUpdated(updated, done);
        });
    });


    it('should have a read model with the updated sanitized description', function (done) {
        repo.skills.findOne({description: newDescription.sanitized}, function(err, found){
            expect(found).to.exist;
            done();
        });
    });

    it('should no longer have a read model with the old sanitized description', function (done) {
        repo.skills.findOne({description: oldDescription.sanitized}, function(err, found){
            expect(found).to.not.exist;
            done();
        });
    });

    after(function(done){
        repo.skills.remove({description: oldDescription.sanitized}, function(){
            repo.skills.remove({description: newDescription.sanitized}, done);
        });
    });

});

