/* jshint -W097 */
'use strict';

var setup = require('./setup'),
    sut = require('../lib/skills'),
    repo = require('../models/repo');

describe('when a user cancels a skill request', function () {
    describe('but has never requested the skill', function () {

        var args = setup.create.skill.args.request(),
            announced = false;

        before(function (done) {
            sut.on('skill-request-cancelled', function(obj){
                announced = obj === args;
            });
            sut.cancelRequest(args, done);
        });

        it("should not announce the skill request has been cancelled", function () {
            announced.should.be.false;
        });
    });

    describe('and has requested the skill', function () {

        var args = setup.create.skill.args.request(),
            announced = false;

        before(function (done) {
            sut.request(args, function(){
                sut.on('skill-request-cancelled', function(obj){
                    announced = obj === args;
                });
                sut.cancelRequest(args, done);
            })
        });

        it('should remove the skill request', function (done) {
            repo.skillRequests.count(args, function(err, count){
                count.should.equal(0);
                done();
            });
        });

        it("should announce the skill request has been cancelled", function () {
            announced.should.be.true;
        });
    });

    describe('without a skill id', function () {
        it('should indicate the arguments are invalid', function (done) {
            sut.cancelRequest({userId: 'xxx'}, function(err){
                expect(err).to.exist;
                done();
            });
        });
    });

    describe('without a user id', function () {
        it('should indicate the arguments are invalid', function (done) {
            sut.cancelRequest({skillId: 'xxx'}, function(err){
                expect(err).to.exist;
                done();
            });
        });
    });
});

