/* jshint -W097 */
'use strict';

var setup = require('./setup'),
    sut = require('../lib/skills'),
    repo = require('../models/repo');

describe('when a user wants to update a skill description', function () {
    describe('and the description does not already exist', function () {

        var args = setup.create.skill.args.updateDescription().literal,
            expected = setup.create.skill.description(args.description).normalized,
            announced = false;

        before(function (done) {
            sut.on('skill-description-updated', function(obj){
                announced = obj === args;
            });
            sut.updateDescription(args, done);
        });

        it('should save the unique skill description in lowercase', function (done) {
            repo.skillDescriptions.findOne(expected, function(err, description){
                expect(description).to.exist;
                done();
            })
        });

        it('should announce that a skill description was updated', function () {
            announced.should.be.true;
        });

        it("should remove the old description", function (done) {
            repo.skillDescriptions.findOne(setup.create.skill.description(args.oldDescription), function(err, description){
                expect(description).to.not.exist;
                done();
            })
        });

        after(function(done){
            repo.skillDescriptions.remove(expected, done);
        })
    });

    describe('and the description already exists', function () {

        var args = setup.create.skill.args.updateDescription().literal,
            existing = setup.create.skill.description(args.description).normalized,
            expected = "'" + args.description + "'" + " already exists.",
            actualErr = null,
            announced = false;

        before(function (done) {
            sut.updateDescription(args, function(err){
                if (err){
                    return done(err);
                }
                sut.on('skill-description-updated', function(obj){
                    announced = obj === args;
                });
                sut.updateDescription(args, function(err){
                    actualErr = err;
                    done();
                });
            });
        });

        it('should not add the description again ', function (done) {
            repo.skillDescriptions.count(existing, function(err, count){
                count.should.equal(1);
                done();
            })
        });

        it('should not announce that a skill description was updated', function () {
            announced.should.be.false;
        });

        it('should return error', function () {
            actualErr.message.should.equal(expected);
        });

        after(function(done){
            repo.skillDescriptions.remove(existing, done);
        })
    });

    describe('without an old description', function () {
        it('should indicate the arguments are invalid', function (done) {
            sut.updateDescription(setup.create.skill.args.updateDescription().withoutOldDescription().literal, function(err){
                expect(err).to.exist;
                err.message.should.contain('Invalid args');
                done();
            });
        });
    });

    describe('with an empty old description', function () {
        it('should indicate the arguments are invalid', function (done) {
            sut.updateDescription(setup.create.skill.args.updateDescription().from('   ').literal, function(err){
                expect(err).to.exist;
                err.message.should.contain('Invalid args');
                done();
            });
        });
    });

    describe('without a new description', function () {
        it('should indicate the arguments are invalid', function (done) {
            sut.updateDescription(setup.create.skill.args.updateDescription().withoutNewDescription().literal, function(err){
                expect(err).to.exist;
                err.message.should.contain('Invalid args');
                done();
            });
        });
    });

    describe('with an empty new description', function () {
        it('should indicate the arguments are invalid', function (done) {
            sut.updateDescription(setup.create.skill.args.updateDescription().to('   ').literal, function(err){
                expect(err).to.exist;
                err.message.should.contain('Invalid args');
                done();
            });
        });
    });

    describe('without a user', function () {
        it('should indicate the arguments are invalid', function (done) {
            sut.updateDescription(setup.create.skill.args.updateDescription().withoutUser().literal, function(err){
                expect(err).to.exist.with;
                err.message.should.contain('Invalid args');
                done();
            });
        });
    });
});

