/* jshint -W097 */
'use strict';

var setup = require('./setup'),
    repo = require('../models/repo'),
    sut = require('../lib/skill.denormalizer');

describe('when a skill request is cancelled', function () {

    var skill = null;

    before(function (done) {
        repo.skills.create(setup.create.skill.doc(), function(err, created){
            skill = created;
            var args = {skillId: created.id};
            sut.handleRequested(args, function(){
                sut.handleRequestCancelled(args, done);
            });
        })
    });

    it('the count should be decremented ', function (done) {
        repo.skills.findById(skill.id, function(err, skill){
            skill.count.should.equal(0);
            done();
        });
    });

    after(function(done){
        skill.remove(done);
    });
});

