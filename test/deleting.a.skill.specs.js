/* jshint -W097 */
'use strict';

var setup = require('./setup'),
    sut = require('../lib/skills'),
    repo = require('../models/repo');

describe('when a user wants to delete a skill', function () {
    describe('and the skill exists', function () {

        var existing = setup.create.skill.args.add(),
            toDelete = setup.create.skill.args.delete(existing.description),
            announced = false;

        before(function (done) {
            sut.add(existing, function(){
                sut.on('skill-deleted', function(obj){
                    announced = obj === toDelete;
                });
                sut.delete(toDelete, done);
            });
        });

        it('should remove the associated skill description', function (done) {
            repo.skillDescriptions.findOne({description: existing.description}, function(err, description){
                expect(description).to.not.exist;
                done();
            })
        });

        it('should announce that a skill was deleted', function () {
            announced.should.be.true;
        });
    });

    describe('and the skill does not exist', function () {

        var args = setup.create.skill.args.delete(),
            announced = false;

        before(function (done) {
            sut.on('skill-deleted', function(obj){
                announced = obj === args;
            });
            sut.delete(args, done);
        });


        it('should not announce that a skill was deleted', function () {
            announced.should.be.false;
        });
    });

    describe('without a description', function () {
        it('should indicate the arguments are invalid', function (done) {
            sut.delete({userId: setup.create.random()}, function(err){
                expect(err).to.exist;
                err.message.should.contain('Invalid args');
                done();
            });
        });
    });

    describe('without an empty description', function () {
        it('should indicate the arguments are invalid', function (done) {
            sut.delete({userId: setup.create.random(), description: '   '}, function(err){
                expect(err).to.exist;
                err.message.should.contain('Invalid args');
                done();
            });
        });
    });

    describe('without a user id', function () {
        it('should indicate the arguments are invalid', function (done) {
            sut.delete({description: setup.create.random()}, function(err){
                expect(err).to.exist.with;
                err.message.should.contain('Invalid args');
                done();
            });
        });
    });
});

