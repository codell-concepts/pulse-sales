/* jshint -W097 */
'use strict';

var setup = require('./setup'),
    sut = require('../lib/skills'),
    repo = require('../models/repo');

describe('when a user gets a skill request from a client', function () {
    describe('and has never requested the skill', function () {

        var args = setup.create.skill.args.request(),
            announced = false;

        before(function (done) {
            sut.on('skill-requested', function(obj){
                announced = obj === args;
            });
            sut.request(args, done);
        });

        it('should save the skill request', function (done) {
            repo.skillRequests.findOne(args, function(err, request){
                expect(request).to.exist;
                done();
            })
        });

        it("should announce that the skill was requested", function () {
            announced.should.be.true;
        });

        after(function(done){
            repo.skillRequests.remove(args, done);
        })
    });

    describe('and has already requested the skill', function () {

        var args = setup.create.skill.args.request(),
            announced = false;

        before(function (done) {
            sut.request(args, function(){
                sut.on('skill-requested', function(obj){
                    announced = obj === args;
                });
                sut.request(args, done);
            })
        });

        it('should not save the same skill request', function (done) {
            repo.skillRequests.count(args, function(err, count){
                count.should.equal(1);
                done();
            });
        });

        it("could be for another client, should still announce that the skill was requested", function () {
            announced.should.be.true;
        });

        after(function(done){
            repo.skillRequests.remove(args, done);
        });
    });

    describe('without a skill id', function () {
        it('should indicate the arguments are invalid', function (done) {
            sut.request({userId: 'xxx'}, function(err){
                expect(err).to.exist;
                done();
            });
        });
    });

    describe('without a user id', function () {
        it('should indicate the arguments are invalid', function (done) {
            sut.request({skillId: 'xxx'}, function(err){
                expect(err).to.exist;
                done();
            });
        });
    });
});

