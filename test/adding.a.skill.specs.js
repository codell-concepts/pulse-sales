/* jshint -W097 */
'use strict';

var setup = require('./setup'),
    sut = require('../lib/skills'),
    repo = require('../models/repo');
describe('when a user wants to add a skill', function () {
    describe('and the description does not already exist', function () {

        var args = setup.create.skill.args.add(),
            expected = setup.create.skill.description(args.description).normalized,
            announced = false,
            announcedArgs = null,
            calledbackId = null;

        before(function (done) {
            sut.on('skill-added', function(obj){
                announced = obj === args;
                announcedArgs = obj;
            });
            sut.add(args, function(err, id){
               calledbackId = id;
               done();
            });
        });

        it('should save the unique skill description in lowercase', function (done) {
            repo.skillDescriptions.findOne(expected, function(err, description){
                expect(description).to.exist;
                done();
            })
        });

        it('should announce that a skill was added', function () {
            announced.should.be.true;
        });

        it('should callback with the id of the added skill', function () {
            calledbackId.should.equal(announcedArgs._id);
        });

        after(function(done){
            repo.skillDescriptions.remove(expected, done);
        })
    });

    describe('and the description already exists', function () {

        var args = setup.create.skill.args.add(),
            existing = setup.create.skill.description(args.description).normalized,
            expected = "'" + args.description + "'" + " already exists.",
            actualErr = null,
            announced = false;

        before(function (done) {
            sut.add(args, function(err){
                if (err){
                    return done(err);
                }
                sut.on('skill-added', function(obj){
                    announced = obj === args;
                });
                sut.add(args, function(err){
                    actualErr = err;
                    done();
                });
            });
        });

        it('should not add the description again ', function (done) {
            repo.skillDescriptions.count(existing, function(err, count){
                count.should.equal(1);
                done();
            })
        });

        it('should not announce that a skill was added', function () {
            announced.should.be.false;
        });

        it('should return error', function () {
            actualErr.message.should.equal(expected);
        });

        it('should return conflict error code', function () {
            actualErr.code.should.equal(409);
        });

        after(function(done){
            repo.skillDescriptions.remove(existing, done);
        })
    });

    describe('without a description', function () {
        it('should indicate the arguments are invalid', function (done) {
            sut.add({userId: 'xxx'}, function(err){
                expect(err).to.exist;
                err.message.should.contain('Invalid args');
                done();
            });
        });
    });

    describe('with an empty description', function () {
        it('should indicate the arguments are invalid', function (done) {
            sut.add(setup.create.skill.args.add('  '), function(err){
                expect(err).to.exist;
                err.message.should.contain('Invalid args');
                done();
            });
        });
    });

    describe('without a user id', function () {
        it('should indicate the arguments are invalid', function (done) {
            sut.add({description: setup.create.random()}, function(err){
                expect(err).to.exist.with;
                err.message.should.contain('Invalid args');
                done();
            });
        });
    });
});

