var chai = require('chai'),
    random = require('randomstring'),
    config = require('config');

global.should = chai.should();
global.expect = chai.expect;

var Skill = function(){
    var me = this;
    me.description = random.generate();
    me.count = 0;
    me.withDescription = function(val){
        me.description = val;
        return me;
    };
    me.requested = function(val){
        me.count = val;
        return me;
    };
    return me;
};

var UpdateSkillDescriptionArgs = function(){
    var me = this;
    me.from = function(val){
        me.literal.oldDescription = val;
        return me;
    };
    me.to = function(val){
        me.literal.description = val;
        return me;
    };
    me.withoutOldDescription = function(){
        delete me.literal.oldDescription;
        return me;
    };
    me.withoutNewDescription = function(){
        delete me.literal.description;
        return me;
    };
    me.withoutUser = function(){
        delete me.literal.userId;
        return me;
    };
    me.literal = {
        userId: random.generate(),
        oldDescription: random.generate(),
        description: random.generate()
    };
    return me;
};

var Description = function(description){
    var me = this,
        value = description || random.generate();
    me.value = value;
    me.sanitized = value.trim();
    me.normalized = value.toLowerCase().trim();
};

var setup = function(){
    return {
        config: config,
        create: {
            random: function(){
              return random.generate();
            },
            skill: {
                args:{
                    request: function(){
                        return {userId: random.generate(), skillId: random.generate()}
                    },
                    add: function(description){
                        return {
                            userId: random.generate(),
                            description: description || random.generate()}
                    },
                    updateDescription: function(){
                        return new UpdateSkillDescriptionArgs();
                    },
                    delete: function(description){
                        return {description: description || random.generate(), userId: random.generate()}
                    }
                },
                doc : function(){
                    return new Skill();
                },
                description: function(description){
                    return new Description(description);
                }
            }
        }
    }
}();

module.exports = setup;