/* jshint -W097 */
'use strict';

var setup = require('./setup'),
    repo = require('../models/repo'),
    sut = require('../lib/skill.denormalizer');

describe('when a skill is requested', function () {

    var skill = null;

    before(function (done) {
        repo.skills.create(setup.create.skill.doc(), function(err, created){
            skill = created;
            sut.handleRequested({skillId: created.id}, done);
        })
    });

    it('the count should be incremented ', function (done) {
        repo.skills.findById(skill.id, function(err, skill){
            skill.count.should.equal(1);
            done();
        });
    });

    after(function(done){
        skill.remove(done);
    });
});

