/* jshint -W097 */
'use strict';

var setup = require('./setup'),
    repo = require('../models/repo'),
    sut = require('../lib/skill.denormalizer');

describe('when a skill is added', function () {

    var description = setup.create.skill.description(' ' + setup.create.random() + ' '),
        added = setup.create.skill.args.add(description.value);

    before(function (done) {
        sut.handleAdded(added, done);
    });

    it('should create a read model with a sanitized description and a count of zero', function (done) {
        repo.skills.findOne({description: description.sanitized}, function(err, found){
            expect(found).to.exist;
            found.count.should.equal(0);
            done();
        });
    });

    after(function(done){
        repo.skills.remove({description: description.sanitized}, done);
    });

});

