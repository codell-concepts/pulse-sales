var gulp = require('gulp'),
    plugins = require('gulp-load-plugins')(),
    code = ['lib/*.js', 'models/*.js', 'index.js'];

gulp.task('test', ['lint'], function(){
    process.env.NODE_ENV = 'tests';
    return gulp.src('./test/*.specs.js', {read: false})
        .pipe(plugins.plumber())
        .pipe(plugins.spawnMocha({
            R : 'spec'
        }));
});

gulp.task('lint', function(){
    return gulp.src(code)
        .pipe(plugins.plumber())
        .pipe(plugins.jshint())
        .pipe(plugins.jshint.reporter('default', {verbose: true}));
});

gulp.task('test-watch', function(){
    gulp.watch(code.concat(['test/**/*.js']), ['test']);
});

gulp.task('run', function(){
   gulp.start(['test-watch']);
});

gulp.task('default', ['run']);
