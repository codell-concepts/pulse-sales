/* jshint -W097 */
'use strict';

var repo = require('./models/repo'),
    skills = require('./lib/skills'),
    denormalizer = require('./lib/skill.denormalizer');

var sales = function(){
    denormalizer.listenTo(skills);
    return {
        skills : {
            requested: function (callback) {
                repo.skills.where('count').gt(0).exec(callback);
            },
            available: function (callback) {
                repo.skills.find({}, callback);
            },
            findById: function(id, callback){
                repo.skills.findById(id, callback);
            },
            findOne: function(args, callback){
                repo.skills.findOne(args, callback);
            },
            request: function (args, callback) {
                skills.request(args, callback);
            },
            cancelRequest: function (args, callback) {
                skills.cancelRequest(args, callback);
            },
            add: function(args, callback) {
                skills.add(args, callback);
            },
            updateDescription: function(args, callback) {
                skills.updateDescription(args, callback);
            },
            delete: function(args, callback){
                skills.delete(args, callback);
            }
        }
    };
}();
module.exports = sales;
