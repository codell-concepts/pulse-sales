var mongoose = require('mongoose');
var schema = new mongoose.Schema({
    userId: String,
    skillId: String,
    timestamp: { type: Date, default: Date.now }
});
module.exports = mongoose.model('SkillRequest', schema, 'SkillRequests');
