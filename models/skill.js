var mongoose = require('mongoose');
var schema = new mongoose.Schema({
    description: String,
    count: Number,
    __v: { type: Number, select: false}
});
module.exports = mongoose.model('Skill', schema, 'Skills');