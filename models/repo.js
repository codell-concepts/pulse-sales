/* jshint -W097 */
'use strict';

var config = require('config'),
    mongoose = require('mongoose'),
    skills = require('./skill'),
    skillRequests = require('./skill.request'),
    skillDescription = require('./skill.description');

var repo = function(){
    mongoose.set('debug', config.debug);
    mongoose.connect(process.env.DB || config.db);
    mongoose.connection.on('error', console.error.bind(console, 'failed to connect to db...'));
    process.on('exit', function(){
        mongoose.disconnect();
    });
    return {
        skills: skills,
        skillRequests: skillRequests,
        skillDescriptions: skillDescription,
        generateId: function(){
            return mongoose.Types.ObjectId();
        }
    };
}();

module.exports = repo;
