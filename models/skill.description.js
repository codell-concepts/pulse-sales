var mongoose = require('mongoose');
var schema = new mongoose.Schema({
    description: String
});
module.exports = mongoose.model('SkillDescription', schema, 'SkillDescriptions');
