/* jshint -W097 */
'use strict';

var validator = require('node-validator'),
    Q = require('q');


var IsEmpty = function(){
    function validate(value, onError){
        if ( ((value + '').trim()).length === 0 )  {
            return onError('Value cannot be empty');
        }
        return null;
    }
    return {
        validate: validate
    };
};


var validators = function(){

    var skillAndUser = {
        check: validator.isObject()
                        .withRequired('skillId')
                        .withRequired('userId'),
        errMessage: 'Invalid args, expected {skillId: x, userId: y}'
    },
    descriptionAndUser = {
        check: validator.isObject()
                        .withRequired('description', new IsEmpty())
                        .withRequired('userId')
                        .withOptional('_id'),
        errMessage: 'Invalid args, expected {description: x, userId: y}'
    },
    updateDescription = {
        check: validator.isObject()
                        .withRequired('oldDescription', new IsEmpty())
                        .withRequired('description', new IsEmpty())
                        .withRequired('userId'),
        errMessage: 'Invalid args, expected {oldDescription: x, description: y, userId: z}'
    };

    function validate(args, rules){
        var deferred = Q.defer();
        validator.run(rules.check, args, function(errCount, errs){
            if (errCount > 0){
                deferred.reject(new Error(rules.errMessage));
            } else {
                deferred.resolve(args);
            }
        });
        return deferred.promise;
    }

    return {
        request : {
            validate: function(args){
                return validate(args, skillAndUser);
            }
        },
        cancel : {
            validate: function(args){
                return validate(args, skillAndUser);
            }
        },
        add : {
            validate: function(args){
                return validate(args, descriptionAndUser);
            }
        },
        updateDescription : {
            validate: function(args){
                return validate(args, updateDescription);
            }
        },
        delete : {
            validate: function(args){
                return validate(args, descriptionAndUser);
            }
        }
    };

}();

module.exports = validators;
