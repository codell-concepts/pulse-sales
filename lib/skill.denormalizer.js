/* jshint -W097 */
'use strict';

var repo = require('../models/repo');

var denormalizer = function(){

    function onRequested(args, callback){
        repo.skills.findByIdAndUpdate(args.skillId, {$inc: {'count' : 1}}, function(err){
            if (callback){
                callback(err);
            }
        });
    }

    function onRequestCancelled(args, callback){
        repo.skills.findByIdAndUpdate(args.skillId, {$inc: {'count' : -1}}, function(err){
            if (callback) {
                callback(err);
            }
        });
    }

    function sanitize(description){
        return description.trim();
    }

    function onAdded(args, callback){
        repo.skills.create({_id: args._id || repo.generateId(), description: sanitize(args.description), count: 0}, function(err){
            if (callback) {
                callback(err);
            }
        });
    }

    function onDescriptionUpdated(args, callback){
        repo.skills.where({description: sanitize(args.oldDescription)}).update({description: sanitize(args.description)}, function(err){
            if (callback) {
                callback(err);
            }
        });
    }

    function onDeleted(args, callback){
        repo.skills.remove({description: sanitize(args.description)}, function(err){
            if (callback) {
                callback(err);
            }
        });
    }

    return {
        handleRequested: onRequested,
        handleRequestCancelled: onRequestCancelled,
        handleAdded: onAdded,
        handleDescriptionUpdated: onDescriptionUpdated,
        handleDeleted: onDeleted,
        listenTo: function(emitter){
            if (emitter) {
                emitter.on('skill-requested', onRequested);
                emitter.on('skill-request-cancelled', onRequestCancelled);
                emitter.on('skill-added', onAdded);
                emitter.on('skill-description-updated', onDescriptionUpdated);
                emitter.on('skill-deleted', onDeleted);
            }
        }
    };
}();

module.exports = denormalizer;
