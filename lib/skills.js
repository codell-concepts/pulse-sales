/* jshint -W097 */
'use strict';

var repo = require('../models/repo'),
    util = require('util'),
    Q = require('q'),
    validators = require('./args.validators'),
    Emitter = require('events').EventEmitter;

var Skills = function(){

    var me = this;
    Emitter.call(me);

    function announce(event, obj, err){
        if (!err) {
            me.emit(event, obj);
        }
        return {
            and: function(callback){
                callback(err);
            }
        };
    }

    function toCriteria(val){
        return {description: val.toLowerCase().trim()};
    }

    function ensureDescriptionUnique(args){
        var deferred = Q.defer();
        repo.skillDescriptions.findOne(toCriteria(args.description), function (err, found) {
            if (err) {
                deferred.reject(err);
            } else if (found) {
                var error = new Error("'" + args.description.trim() + "'" + " already exists.");
                error.code = 409;
                deferred.reject(error);
            } else {
                deferred.resolve(args);
            }
        });
        return deferred.promise;
    }

    function removeOldDescription(args){
        var deferred = Q.defer();
        repo.skillDescriptions.remove(toCriteria(args.oldDescription), function(err){
            if(err){
                deferred.reject(err);
            } else {
                deferred.resolve(args);
            }
        });
        return deferred.promise;
    }

    function addDescription(args){
        return repo.skillDescriptions.create(toCriteria(args.description));
    }

    me.request = function(args, callback){
        validators.request
            .validate(args)
            .then(function(args){
                return repo.skillRequests.findOneAndUpdate(args, {}, {upsert: true}).exec();
            })
            .then(function(){
                announce('skill-requested', args).and(callback);
            })
            .catch(callback);
    };

    me.cancelRequest = function(args, callback){
        validators.cancel
            .validate(args)
            .then(function (args) {
                repo.skillRequests.remove(args, function (err, removed) {
                    if (removed.result.n > 0) {
                        announce('skill-request-cancelled', args, err).and(callback);
                    }
                    else {
                        callback(err);
                    }
                });
            })
            .catch(callback);
    };

    me.add = function(args, callback){
        args._id = args._id || repo.generateId();
        validators.add
            .validate(args)
            .then(ensureDescriptionUnique)
            .then(addDescription)
            .then(function(){
                me.emit('skill-added', args);
            }).then(function(){
                callback(null, args._id);
            })
            .catch(callback);
    };

    me.updateDescription = function(args, callback){
        validators.updateDescription
            .validate(args)
            .then(ensureDescriptionUnique)
            .then(removeOldDescription)
            .then(addDescription)
            .then(function(){
                announce('skill-description-updated', args).and(callback);
            })
            .catch(callback);
    };

    me.delete = function(args, callback){
        validators.delete
            .validate(args)
            .then(function(){
                repo.skillDescriptions.remove(toCriteria(args.description), function(err, removed){
                    if(removed.result.n > 0){
                        announce('skill-deleted', args, err).and(callback);
                    } else {
                        callback(err);
                    }
                });
            })
            .catch(callback);
    };
};

util.inherits(Skills, Emitter);
module.exports = new Skills();
